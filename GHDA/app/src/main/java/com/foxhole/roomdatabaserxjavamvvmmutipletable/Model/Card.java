package com.foxhole.roomdatabaserxjavamvvmmutipletable.Model;

import android.os.Parcel;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import javax.xml.namespace.QName;

public class Card {

    private String uid;
    private String name;
    private String upperProperty;
    private String initiative;
    private String lowerProperty;
    private String  level;
    private String  clazz_id;

    public String getClazz_id() {
        return clazz_id;
    }

    public void setClazz_id (String clazz_id) {
        this.clazz_id = clazz_id;
    }

    public String getInitiative() {
        return initiative;
    }

    public void setInitiative(String initiative) {
        this.initiative = initiative;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getLowerProperty() {
        return lowerProperty;
    }

    public void setLowerProperty(String lowerProperty) {
        this.lowerProperty = lowerProperty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUpperProperty() {
        return upperProperty;
    }

    public void setUpperProperty(String upperProperty) {
        this.upperProperty = upperProperty;
    }

}

