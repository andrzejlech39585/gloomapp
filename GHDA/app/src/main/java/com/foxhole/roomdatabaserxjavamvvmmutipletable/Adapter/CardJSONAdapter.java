package com.foxhole.roomdatabaserxjavamvvmmutipletable.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.foxhole.roomdatabaserxjavamvvmmutipletable.Model.Card;
import com.foxhole.roomdatabaserxjavamvvmmutipletable.R;

import java.util.ArrayList;

public class CardJSONAdapter extends BaseAdapter {

    Context context;
    ArrayList<Card> arrayList;

    public CardJSONAdapter(Context context, ArrayList<Card> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public  View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView ==  null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.custom_row, parent, false);
        }
        TextView name;
        name = convertView.findViewById(R.id.nameItem);
        name.setText(arrayList.get(position).getName());

        return convertView;
    }
}