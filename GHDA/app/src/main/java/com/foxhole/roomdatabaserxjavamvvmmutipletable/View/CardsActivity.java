package com.foxhole.roomdatabaserxjavamvvmmutipletable.View;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;



import androidx.appcompat.app.AppCompatActivity;

import com.foxhole.roomdatabaserxjavamvvmmutipletable.Adapter.CardJSONAdapter;
import com.foxhole.roomdatabaserxjavamvvmmutipletable.Model.Card;
import com.foxhole.roomdatabaserxjavamvvmmutipletable.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class CardsActivity extends AppCompatActivity {

    ListView listView;
    ArrayList<Card> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_layout);
        listView = (ListView) findViewById(R.id.list);
        arrayList = new ArrayList<>();

        TextView title = findViewById(R.id.ClayoutTitle);
        title.setText("Karty");

        Button bb = findViewById(R.id.backButton);
        bb.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), PartyActivity.class);
                startActivity(i);
                finish();
            }
        });

        try {
            JSONObject object = new JSONObject(readJSON("Cards.json"));
            JSONArray array = object.getJSONArray("data");
            for (int i = 0; i < array.length(); i++) {

                JSONObject jsonObject = array.getJSONObject(i);
                String classId = jsonObject.getString("ClassID");
                String id = jsonObject.getString("CardID");
                String name = jsonObject.getString("CardName");
                String upperProperty = jsonObject.getString("UpperProperty");
                String initiative = jsonObject.getString("Initiative");
                String lowerProperty = jsonObject.getString("LowerProperty");
                String level = jsonObject.getString("CardLevel");

                Card model = new Card();
                model.setName(name);
                model.setClazz_id(classId);
                model.setInitiative(initiative);
                model.setLevel(level);
                model.setUpperProperty(upperProperty);
                model.setLowerProperty(lowerProperty);
                arrayList.add(model);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final CardJSONAdapter adapter = new CardJSONAdapter(this, arrayList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent i2 = new Intent(CardsActivity.this, CardDetails.class);

                Bundle args = new Bundle();
                args.putString("name",arrayList.get(position).getName());
                args.putString("level",arrayList.get(position).getLevel());
                args.putString("class",arrayList.get(position).getClazz_id());
                args.putString("initiative",arrayList.get(position).getInitiative());
                args.putString("upperProperty",arrayList.get(position).getUpperProperty());
                args.putString("lowerProperty",arrayList.get(position).getLowerProperty());

                i2.putExtra("BUNDLE", args);
                startActivity(i2);
            }
        });
    }

    public String readJSON(String filename) {
        String json = null;
        try {

            InputStream inputStream = getAssets().open(filename);
            int size = inputStream.available();
            byte[] buffer = new byte[size];

            inputStream.read(buffer);
            inputStream.close();

            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return json;
        }
        return json;
    }
}



