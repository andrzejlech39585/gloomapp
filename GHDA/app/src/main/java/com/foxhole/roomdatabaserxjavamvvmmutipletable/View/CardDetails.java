package com.foxhole.roomdatabaserxjavamvvmmutipletable.View;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.foxhole.roomdatabaserxjavamvvmmutipletable.R;

public class CardDetails extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.card_details);

        Button bb = findViewById(R.id.backButton);
        bb.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), CardsActivity.class);
                startActivity(i);
                finish();
            }
        });


        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");

        TextView title = findViewById(R.id.ClayoutTitle);
        TextView level = findViewById(R.id.Clevel);
        TextView initiative = findViewById(R.id.Cinitiative);
        TextView name = findViewById(R.id.CclassName);
        TextView lp = findViewById(R.id.ClowerProperty);
        TextView up = findViewById(R.id.CupperProperty);

        title.setText(args.getString("name"));
        level.setText(args.getString("level"));
        initiative.setText(args.getString("initiative"));
        up.setText(args.getString("upperProperty"));
        lp.setText(args.getString("lowerProperty"));
        name.setText(args.getString("class"));

        switch (name.getText().toString()) {
            case "1":
                name.setText("Sawas Skałosercy"); break;
            case "2":
                name.setText("Orchidea Tkaczka Zaklęć"); break;
            case "3":
                name.setText("Szczurok Myślołap"); break;
            case "4":
                name.setText("Inoks Kark"); break;
            case "5":
                name.setText("Człowiek Szelma"); break;
            case "6":
                name.setText("Kwatryl Druciarz"); break;

        }

    }
}
